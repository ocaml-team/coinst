.TH COINST 1

.SH NAME
Coinst \- computes the co-installability kernel of a package repostory

.SH SYNOPSIS
.B coinst [options]

.SH DESCRIPTION
.B coinst
reads a package repository in debian format or RPM format from
standard input, and computes the co-installability kernel of the
repository (see below). The graph of the kernel is written in dot
format to a file, and a diagnostic is written to standard output. This
diagnostic consists by default of:
.RS 2
.IP "\-" 2
the list of equivalence classes of packages
.IP "\-" 2
the list of non-installable packages
.IP "\-" 2
the list of not co-installable pairs of packages
.RE

.SH CO-INSTALLABILITY KERNELS
A set of packages is called co-installable with respect to a
repository R if it can be extended to a set of packages from R that
satisfies all inter-package relations (dependencies, conflicts,
etc.). In particular, a package p is installable if the set {p}
consisting of p only is co-installable.

This tool reduces a repository R to a much smaller one, its so-called
co-installability kernel R', that behaves exactly the same as far as
co-installability of package is concerned: Any set of packages P is
co-installable with respect to P iff it co-installable with respect to
R'. This is achieved by
.RS 2
.IP "\-" 2
dropping all relations that are not relevant for this purpose. For
instance, dependencies that do not lead directly or indirectly to any
conflicts are dropped.
.IP "\-" 2
identifying all packages that behave the same. For instance, packages
that are not in conflict with any other package (even not through
dependency chains) behave the same since they are co-installable
together with any other co-installable set of packages, and packages
that are not installable at all behave the same since they may never be
part of any co-installable set of packages.
.RE

A more precise explanation can be found in the original research article
underlying this tool.

The interest of computing the kernel is that it is typically orders of
magnitude smaller than the original repository.

.SH OPTIONS

.SS Options controlling the input
.TP
.B \-deb
expect input in the format of a debian Packages file (default).
.TP
.B \-rpm
expect input in the format of an RPM hdlist.cz file.
.TP
.B \-ignore \fIpackage\fB
ignore the package named \fIpackage\fR.

.SS Options controlling the graph output
.TP
.B \-o \fIfile\fB
write the graph to \fIfile\fR instead of graph.dot
.TP
.B \-all
include all packages in the coinstallability graph
.TP
.B \-root \fIp\fR
draw only the relevant portion of the graph around package \fIp\fR.

.SS Options controlling the diagnostic output
.TP
.B \-explain
explain the list of non-installable pairs of packages.
.TP
.B \-stats
show statistics regarding the input and output repositories

.SS Miscellaneous options
.TP
.B \-help, \-\-help
show command synopsis

.SH EXAMPLE
Reduce a current debian Packages file to its kernel:

   coinst -all -o raw.dot < sid_main_binary-amd64_Packages

Layout the graph:

   dot raw.dot -o layout.dot:

View the graph with dotty, or the viewer from the coinst_viewer package:

   dotty layout.dot
   coinst_viewer layout.dot

.SH AUTHOR
Coinst has been written by Jérome Vouillôn.  This manpage has been
compiled by Ralf Treinen from the original coinst documentation.

.SH SEE ALSO
.BR dot (1), dotty (1), coinst_viewer (1)
.br
.I http://coinst.irill.org
.br
The original research article decribing the algorithm behind this tool
is Roberto Di Cosmo and Jérôme Vouillon, \fIOn software component
co-installability\fR, 19th ACM SIGSOFT Symposium on the Foundations of
Software Engineering (FSE-19) and ESEC'11: 13rd European Software
Engineering Conference (ESEC-13), Szeged, Hungary, September 5-9, 2011,
pages 256-266.
